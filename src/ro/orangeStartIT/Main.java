package ro.orangeStartIT;

import java.util.Arrays;
import java.util.List;

//numeric streams
public class Main {

    public static void main(String[] args) {

    List<Item> items = Arrays.asList(
            new Item("towels", 20),
            new Item("sheets", 15)
    );  //Create a List of type Item and add towels and sheets to List

        int quantity = items.stream()
                        .mapToInt(q -> q.getQuantity()).sum();
        System.out.println(quantity);
    }
}
